#This is a test input file for Hackathon 2015 problem 2.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 50
  ny = 50
  xmin = 0
  xmax = 50
  ymin = 0
  ymax = 50

  elem_type = QUAD4
[]

[GlobalParams]
  well_width_factor = 1.414216
  alpha = 5
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonConcIC
      epsilon = 0.05
      base_value = 0.5
      q1 = '0.105 0.11 0'
      q2 = '0.13 0.087 0'
      q3 = '0.025 0.15 0'
      q4 = '0.07 0.02 0'
      constant = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./n1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.01 0.017 0'
      q2 = '0.12 0.12 0'
      q3 = '0.047 0.0415 0'
      q4 = '0.032 0.005 0'
      h = 1.5
    [../]
  [../]

  [./n2]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.02 0.027 0'
      q2 = '0.13 0.13 0'
      q3 = '0.048 0.0425 0'
      q4 = '0.033 0.006 0'
      h = 1.5
    [../]
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = FIRST
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = CHCoupledSplitHackathon
    variable = c
    w = mu
    kappa_name = kappa_CH
    OP_var_names = 'n1 n2'
   # well_width_factor = 1.414216
  [../]

  [./ACBulk_n1]
    type = ACBulkPolyCoupledHackathon
    variable = n1
    OP_var_names = 'n1 n2'
    OP_number = 1
    coupled_CH_var = c
    #well_width_factor = 1.414216
    #alpha = 10
  [../]

  [./deta1dt]
    type = TimeDerivative
    variable = n1
  [../]

  [./ACinterface1]
    type = IsotropicACInterface
    variable = n1
  [../]
#####
  [./ACBulk_n2]
    type = ACBulkPolyCoupledHackathon
    variable = n2
    OP_var_names = 'n1 n2'
    OP_number = 2
    coupled_CH_var = c
   #well_width_factor = 1.414216
   # alpha = 10
  [../]

  [./deta2dt]
    type = TimeDerivative
    variable = n2
  [../]

  [./ACinterface2]
    type = IsotropicACInterface
    variable = n2
  [../]
[]

[AuxKernels]
   [./total_energy_calc]
      type = HackathonCHACEnergy
      variable = total_energy
      CH_var = c
      OP_var_names = 'n1 n2'
    #  well_width_factor = 1.414216
    #   alpha = 10
   [../]
[]

[Materials]
  [./generic]
    type = HackathonMaterial
    CH_mobility = 5
    AC_mobility = 5
    w = 1
    kappa_CH = 3
    kappa_AC = 3
    c_alpha = 0.3
    c_beta = 0.7
    concentration = c
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = ConstantDT
    dt = 1e-2
  [../]

  num_steps = 3
  dtmin = 1e-3

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -sub_pc_type'
  petsc_options_value = ' ksp      lu        '


  l_max_its = 100
 # nl_max_its = 10
  nl_abs_tol = 1e-11
[]

[Outputs]
  exodus = true
  interval = 1
  csv = false
  file_base = hackathon_p2

  checkpoint = false
  [./console]
    type = Console
    perf_log = true
  [../]
[]
