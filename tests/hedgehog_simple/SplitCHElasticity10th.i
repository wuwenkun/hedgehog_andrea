#This tests the SplitCHCobalt (chemical energy) kernel and CobaltTESTMaterial.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 25
  ny = 25
  xmin = 25
  xmax = 50
  ymin = 25
  ymax = 50

  elem_type = QUAD4
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.01
      radius = 10
      int_width = 5
      x1 = 25
      y1 = 25
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_x]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_y]
    order = FIRST
    family = LAGRANGE
  [../]

[]

[AuxVariables]
  [./bulk_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./interface_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./elastic_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./total_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./elastic_interaction_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s11_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s12_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s22_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e11_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e12_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e22_aux]
    order = FIRST
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./TensorMechanics]
    displacements = 'disp_x disp_y'
  [../]

  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltElasticity
    variable = eta1
    w = mu
    kappa_name = kappa_AC
    use_elasticity = true
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_elastic_energy]
    type = ElasticEnergy
    variable = elastic_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    elastic_energy = elastic_energy
    variable = total_energy
  [../]

  [./aux_elastic_interaction_energy]
    type = ElasticInteractionEnergy
    variable = elastic_interaction_energy
  [../]

  [./matl_s11]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 0
    variable = s11_aux
  [../]

 [./matl_s12]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 1
    variable = s12_aux
  [../]

  [./matl_s22]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 1
    index_j = 1
    variable = s22_aux
  [../]

  [./matl_e11]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 0
    variable = e11_aux
  [../]

  [./matl_e12]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 1
    variable = e12_aux
  [../]

  [./matl_e22]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 1
    index_j = 1
    variable = e22_aux
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltTestMaterial
    AC_mobility = 5
    well_height = 1.95e-1
    kappa_AC = 5.8e-1

    polynomial_order = 10
    coefficients = '230.2006355 -1151.003178 2506.663551 -3120.635139 2444.046270 -1244.129167 408.0297321 -81.24549382 8.072789087 0 0'

    order_parameter = eta1
  [../]

 [./gamma_gammaprime]
    type = HeterogeneousLinearElasticMaterial
    #reading C_11 C_12 C_13 C_22 C_23 C_33 C_44 C_55 C_66
    #units of aJ/nm^3
    Cijkl_matrix = '229 165 165 229 165 229 97.3 97.3 97.3'
    Cijkl_precip = '272 158 158 272 158 272 162 162 162'
    precipitate_eigenstrain = '0.0053 0.0053 0.0053 0 0 0'

    order_parameter = eta1
    disp_x = disp_x
    disp_y = disp_y
  [../]
[]

[VectorPostprocessors]
  [./feature_volumes]
    type = FeatureVolumeVectorPostprocessor
    flood_counter = feature_counter
    outputs = none
    execute_on = timestep_end
  [../]
[]

[Postprocessors]
  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
  [./SystemFreeEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
 [./numDOFs]
    type = NumDOFs
  [../]
  [./feature_counter]
    type = FeatureFloodCount
    variable = eta1
    threshold = 0.5
    compute_var_to_feature_map = true
    use_displaced_mesh = false
    execute_on = timestep_end
  [../]
  [./VolumeFraction]
    type = FeatureVolumeFraction
    mesh_volume = Volume
    feature_volumes = feature_volumes
    use_displaced_mesh = false
    execute_on = timestep_end
  [../]
[]

[BCs]
 [./pin_nodex]
    type = DirichletBC
    variable = disp_x
    value = 0.0
    boundary = 'left'
  [../]

 [./pin_nodey]
    type = DirichletBC
    variable = disp_y
    value = 0.0
    boundary = 'bottom'
  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

 [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e1
    cutback_factor = 0.25
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 2
  dtmin = 1e-2
  #dtmax = 1e2

  solve_type = 'PJFNK'
#  petsc_options_iname = '-pc_type -sub_pc_type'
#  petsc_options_value = ' asm      lu         '
  petsc_options_iname = '-pc_type -sub_pc_type'
  petsc_options_value = 'ksp       lu'

  l_tol = 1e-4
  l_max_its = 50
  nl_max_its = 10
  nl_rel_tol = 1e-6
  nl_abs_tol = 1e-10

[]


[Outputs]
  interval = 1
  csv = false
  file_base = SplitCHElasticity10th
  checkpoint = false

  [./console]
    type = Console
    perf_log = true
  [../]

  [./exodus]
    type = Exodus
    use_problem_dimension = false
  [../]
[]
