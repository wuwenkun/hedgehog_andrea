#This tests the SplitCHCobalt (chemical energy) kernel and CobaltTESTMaterial.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 50
  ny = 50
  xmin = 0
  xmax = 50
  ymin = 0
  ymax = 50

  elem_type = QUAD4
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.01
      radius = 10
      int_width = 5
      x1 = 25
      y1 = 25
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./interface_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./total_energy]
    order = FIRST
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltBase
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    elastic_energy = 0
    variable = total_energy
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltTestMaterial
    AC_mobility = 5
    well_height = 1.95e-1
    kappa_AC = 5.8e-1

    polynomial_order = 10
    coefficients = '230.2006355 -1151.003178 2506.663551 -3120.635139 2444.046270 -1244.129167 408.0297321 -81.24549382 8.072789087 0 0'

    order_parameter = eta1
  [../]
[]

[VectorPostprocessors]
  [./feature_volumes]
    type = FeatureVolumeVectorPostprocessor
    flood_counter = feature_counter
    outputs = none
    execute_on = timestep_end
  [../]
[]

[Postprocessors]
  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
  [./SystemFreeEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
 [./numDOFs]
    type = NumDOFs
  [../]
  [./feature_counter]
    type = FeatureFloodCount
    variable = eta1
    threshold = 0.5
    compute_var_to_feature_map = true
    use_displaced_mesh = false
    execute_on = timestep_end
  [../]
  [./VolumeFraction]
    type = FeatureVolumeFraction
    mesh_volume = Volume
    feature_volumes = feature_volumes
    use_displaced_mesh = false
    execute_on = timestep_end
  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

 [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e1
    cutback_factor = 0.25
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 2
  dtmin = 1e-2
  #dtmax = 1e2

  solve_type = 'PJFNK'
#  petsc_options_iname = '-pc_type -sub_pc_type'
#  petsc_options_value = ' asm      lu         '
  petsc_options_iname = '-pc_type -sub_pc_type'
  petsc_options_value = 'ksp       lu'

  l_tol = 1e-4
  l_max_its = 50
  nl_max_its = 10
  nl_rel_tol = 1e-6
  nl_abs_tol = 1e-10

[]


[Outputs]
  interval = 1
  csv = false
  file_base = SplitCHChem10th
  checkpoint = false

  [./console]
    type = Console
    perf_log = true
  [../]

  [./exodus]
    type = Exodus
    use_problem_dimension = false
  [../]
[]
