/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   5 February 2016
 *
 *****************************************************/

#include "SplitCHCobaltBase.h"

template<>
InputParameters validParams<SplitCHCobaltBase>()
{
  InputParameters params = validParams<SplitCHCRes>();
  //add things as necessary

  return params;
}

SplitCHCobaltBase::SplitCHCobaltBase(const InputParameters & parameters) :
    SplitCHCRes(parameters),
    _df_dOP(getMaterialProperty<Real>("df_bulk_dOP")),
    _d2f_dOP2(getMaterialProperty<Real>("d2f_bulk_dOP2"))
{
}

Real
SplitCHCobaltBase::computeDFDC(PFFunctionType type)
{
  switch (type)
  {
    case Residual:
      return _df_dOP[_qp];

    case Jacobian:
      return _phi[_j][_qp]*_d2f_dOP2[_qp];
  }

  mooseError("Invalid type passed in");
}

//Real
//SplitCHCobaltBase::computeDEDC(PFFunctionType type)
//{
//  return 0;
//}
