/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   20 April 2016
 *
 *****************************************************/

#include "ElasticInteractionEnergy.h"

template<>
InputParameters validParams<ElasticInteractionEnergy>()
{
  InputParameters params = validParams<AuxKernel>();

  return params;
}

ElasticInteractionEnergy::ElasticInteractionEnergy(const InputParameters & parameters) :
    AuxKernel(parameters),
    _felasticInt(getMaterialProperty<Real>("f_elasticInt"))
{
}

Real
ElasticInteractionEnergy::computeValue()
{
  return _felasticInt[_qp];
}
