/******************************************************
*
*   Welcome to Hedgehog!
*
*   CHiMaD (ANL/Northwestern University)
*
*   Developer: Andrea Jokisaari
*
*   22 August 2016
*
*****************************************************/

#include "Blob3IC.h"

#include "libmesh/point.h"
#include <cmath>

template<>
InputParameters validParams<Blob3IC>()
{
  InputParameters params = validParams<InitialCondition>();
  params.addRequiredParam<Real>("a", "length of a-axis");
  params.addRequiredParam<Real>("b", "length of b-axis");
  params.addParam<Real>("c", 1, "length of c-axis");
  params.addRequiredParam<Real>("invalue", "value inside the particle");
  params.addRequiredParam<Real>("outvalue", "value outside the particle");
  params.addParam<Real>("int_width", 0, "diffuse interface width");

  return params;
}

Blob3IC::Blob3IC(const InputParameters & parameters) :
    InitialCondition(parameters),
    _a(getParam<Real>("a")),
    _b(getParam<Real>("b")),
    _c(getParam<Real>("c")),
    _invalue(getParam<Real>("invalue")),
    _outvalue(getParam<Real>("outvalue")),
    _int_width(getParam<Real>("int_width"))
{
}

Real
Blob3IC::value(const Point & p)
{

  //This is a pretty poorly coded initial condition.  It really only is designed for 2D
  //and a particle centered at (0,0)

  Real blob_positive = std::sqrt(_a*_a*(1 - p(0)*p(0)/(_b*_b)) + 7000*std::sin(0.03*p(0)) + 7000*std::cos(0.04*p(0)));

  Real blob_negative = -1*std::sqrt(_a*_a*(1.05 -((p(0) - 15)*(p(0) - 15))/(_b*_b)) + 7000*std::sin(0.033*p(0)) + 7000*std::cos(0.047*p(0)));


  Real d = _int_width/2;

  Real value = 0;


  if ( p(0) > 300.0 || p(0) < -300.0 )
    value = _outvalue;

  else if ( p(1) <  blob_positive + d &&  p(1) >  blob_positive - d)
   {
     Real int_pos = (p(1) - blob_positive + d)/_int_width;
     value = _outvalue + (_invalue - _outvalue)*(1+std::cos(int_pos*libMesh::pi))/2;
   }

   else if ( p(1) >  blob_negative - d &&  p(1) <  blob_negative + d)
   {
     Real int_pos = (p(1) - blob_negative + d)/_int_width;
     value = _outvalue + (_invalue - _outvalue)*(1-std::cos(int_pos*libMesh::pi))/2;
   }

   else if ( p(1) > blob_positive + d || p(1) < blob_negative - d )
     value = _outvalue;

   else
     value = _invalue;

return value;


}
