#include "HedgehogApp.h"
#include "Moose.h"
#include "Factory.h"
#include "AppFactory.h"
#include "MooseSyntax.h"

//-- Hedgehog Includes

//Specific Modules
#include "TensorMechanicsApp.h"
#include "PhaseFieldApp.h"

//-- Kernels
#include "IsotropicACBulk.h"
#include "ACCobaltTest.h"
#include "IsotropicACInterface.h"
#include "SplitCHCobaltBase.h"
#include "SplitCHCobaltElasticity.h"
#include "CHSplitHackathon.h"
#include "ACBulkPolyCoupledHackathon.h"
#include "CHCoupledSplitHackathon.h"

//-- Materials
#include "CobaltBaseMaterial.h"
#include "HeterogeneousLinearElasticMaterial.h"
#include "HackathonMaterial.h"
#include "CobaltTestMaterial.h"

//-- Auxkernels
#include "BulkFreeEnergy.h"
#include "ElasticEnergy.h"
#include "InterfaceFreeEnergy.h"
#include "FreeEnergyTotal.h"
#include "GrandPotential.h"
#include "HackathonCHEnergy.h"
#include "HackathonCHACEnergy.h"
#include "ElasticInteractionEnergy.h"

//-- Initial conditions
#include "HackathonConcIC.h"
#include "HackathonEtaIC.h"
#include "BlobIC.h"
#include "Blob2IC.h"
#include "Blob3IC.h"

//-- Boundary conditions
#include "StressBC.h"

template<>
InputParameters validParams<HedgehogApp>()
{
  InputParameters params = validParams<MooseApp>();

  params.set<bool>("use_legacy_uo_initialization") = false;  // add new input para
  params.set<bool>("use_legacy_uo_aux_computation") = false;
  params.set<bool>("use_legacy_output_syntax") = false;

  return params;
}

HedgehogApp::HedgehogApp(InputParameters parameters) :
    MooseApp(parameters)
{
  Moose::registerObjects(_factory);
  HedgehogApp::registerObjects(_factory);

  TensorMechanicsApp::registerObjects(_factory);
  TensorMechanicsApp::associateSyntax(_syntax, _action_factory);

  PhaseFieldApp::registerObjects(_factory);
  PhaseFieldApp::associateSyntax(_syntax, _action_factory);


  Moose::associateSyntax(_syntax, _action_factory);
  HedgehogApp::associateSyntax(_syntax, _action_factory);
}

HedgehogApp::~HedgehogApp()
{
}

// External entry point for dynamic application loading
extern "C" void HedgehogApp__registerApps() { HedgehogApp::registerApps(); }
void
HedgehogApp::registerApps()
{
  registerApp(HedgehogApp);
}

// External entry point for dynamic object registration
extern "C" void HedgehogApp__registerObjects(Factory & factory) { HedgehogApp::registerObjects(factory); }
void
HedgehogApp::registerObjects(Factory & factory)
{
  registerKernel(IsotropicACBulk);
  registerKernel(ACCobaltTest);
  registerKernel(IsotropicACInterface);
  registerKernel(SplitCHCobaltBase);
  registerKernel(SplitCHCobaltElasticity);
  registerKernel(CHSplitHackathon);
  registerKernel(ACBulkPolyCoupledHackathon);
  registerKernel(CHCoupledSplitHackathon);

  registerMaterial(CobaltBaseMaterial);
  registerMaterial(HeterogeneousLinearElasticMaterial);
  registerMaterial(HackathonMaterial);
  registerMaterial(CobaltTestMaterial);


  registerAuxKernel(BulkFreeEnergy);
  registerAuxKernel(ElasticEnergy);
  registerAuxKernel(InterfaceFreeEnergy);
  registerAuxKernel(FreeEnergyTotal);
  registerAuxKernel(GrandPotential);
  registerAuxKernel(HackathonCHEnergy);
  registerAuxKernel(HackathonCHACEnergy);
  registerAuxKernel(ElasticInteractionEnergy);

  registerInitialCondition(HackathonConcIC);
  registerInitialCondition(HackathonEtaIC);
  registerInitialCondition(BlobIC);
  registerInitialCondition(Blob2IC);
  registerInitialCondition(Blob3IC);


  registerBoundaryCondition(StressBC);
}

// External entry point for dynamic syntax association
extern "C" void HedgehogApp__associateSyntax(Syntax & syntax, ActionFactory & action_factory) { HedgehogApp::associateSyntax(syntax, action_factory); }
void
HedgehogApp::associateSyntax(Syntax & /*syntax*/, ActionFactory & /*action_factory*/)
{
}
