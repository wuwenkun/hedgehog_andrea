/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   10 February 2016
 *
 *****************************************************/

#include "HeterogeneousLinearElasticMaterial.h"
#include "MooseMesh.h"
#include <cmath>

template<>
InputParameters validParams<HeterogeneousLinearElasticMaterial>()
{
  InputParameters params = validParams<Material>();

  params.addRequiredParam<std::vector<Real> >("Cijkl_matrix", "Stiffness tensor of matrix");
  params.addParam<MooseEnum>("matrix_fill_method", RankFourTensor::fillMethodEnum() = "symmetric9", "Cijkl_matrix fill method");
  params.addRequiredParam<std::vector<Real> >("Cijkl_precip", "Stiffness tensor of precip");
  params.addParam<MooseEnum>("precip_fill_method", RankFourTensor::fillMethodEnum() = "symmetric9", "Cijkl_precip fill method");

  params.addRequiredParam<std::vector<Real> >("precipitate_eigenstrain", "eigenstrain between precip and matrix");

  params.addRequiredCoupledVar("order_parameter", "order parameter variable");
  params.addRequiredCoupledVar("disp_x", "The x displacement");
  params.addRequiredCoupledVar("disp_y", "The y displacement");
  params.addCoupledVar("disp_z", "The z displacement");

  return params;
}

HeterogeneousLinearElasticMaterial::HeterogeneousLinearElasticMaterial(const InputParameters & parameters) :
    Material(parameters),
    _OP(coupledValue("order_parameter")),
    _grad_disp_x(coupledGradient("disp_x")),
    _grad_disp_y(coupledGradient("disp_y")),
    _grad_disp_z(_mesh.dimension() == 3 ? coupledGradient("disp_z") : _grad_zero),
    _h(0),
    _dhdn(0),
    _d2hdn2(0),
    _stress(declareProperty<RankTwoTensor>("stress")),
    _total_strain(declareProperty<RankTwoTensor>("total_strain")),
    _elastic_strain(declareProperty<RankTwoTensor>("elastic_strain")),
    _misfit_strain(declareProperty<RankTwoTensor>("misfit_strain")),
    _elasticity_tensor(declareProperty<RankFourTensor>("elasticity_tensor")),
    _Jacobian_multiplier(declareProperty<RankFourTensor>("Jacobian_mult")),
    _f_el(declareProperty<Real>("f_elastic")),
    _dfel_dOP(declareProperty<Real>("dfel_dOP")),
    _d2fel_dOP2(declareProperty<Real>("d2fel_dOP2")),
    _f_el_int(declareProperty<Real>("f_elasticInt")),
    _Cijkl_matrix(getParam<std::vector<Real> >("Cijkl_matrix"), (RankFourTensor::FillMethod)(int)getParam<MooseEnum>("matrix_fill_method")),
    _Cijkl_precip(getParam<std::vector<Real> >("Cijkl_precip"), (RankFourTensor::FillMethod)(int)getParam<MooseEnum>("precip_fill_method")),
    _eigenstrain_vector(getParam<std::vector<Real> >("precipitate_eigenstrain"))
{
  _eigenstrain.fillFromInputVector(_eigenstrain_vector);
}

void
HeterogeneousLinearElasticMaterial::computeQpProperties()
{
  computeInterpolation();

  computeQpElasticityTensor();

  computeQpStrain();

  computeQpStress();

  computeEnergy();

}

void
HeterogeneousLinearElasticMaterial::computeInterpolation()
{
  _h = _OP[_qp]*_OP[_qp]*_OP[_qp]*(6*_OP[_qp]*_OP[_qp] - 15*_OP[_qp] + 10);
  _dhdn = 30*_OP[_qp]*_OP[_qp]*( _OP[_qp] - 1)*( _OP[_qp] - 1);
  _d2hdn2 = 60*_OP[_qp]*( _OP[_qp] - 1)*(2*_OP[_qp] - 1);

  //_h = 0.5*( std::tanh(30*(_OP[_qp]- 0.5)) + 1);
  //_dhdn = 15*( std::pow( 1/std::cosh(30*(_OP[_qp] - 0.5)), 2.0 ));
  //_d2hdn2 = -60*_dhdn*std::tanh(30*(_OP[_qp] - 0.5));
}

void
HeterogeneousLinearElasticMaterial::computeQpElasticityTensor()
{
  _elasticity_tensor[_qp] = _Cijkl_matrix*(1 - _h) + _Cijkl_precip*_h;
  _Jacobian_multiplier[_qp] = _elasticity_tensor[_qp];

  // _Jacobian_multiplier[_qp] = _Cijkl_matrix*(1 - _h) + _Cijkl_precip*_h;
}

void
HeterogeneousLinearElasticMaterial::computeQpStrain()
{
  RankTwoTensor grad_tensor(_grad_disp_x[_qp], _grad_disp_y[_qp], _grad_disp_z[_qp]);

  _total_strain[_qp] = 0.5*(grad_tensor + grad_tensor.transpose());
  _misfit_strain[_qp] = _eigenstrain*_h;
  _elastic_strain[_qp] = _total_strain[_qp] - _misfit_strain[_qp];
}

void
HeterogeneousLinearElasticMaterial::computeQpStress()
{
  _stress[_qp] = _elasticity_tensor[_qp]*_elastic_strain[_qp];
}

void
HeterogeneousLinearElasticMaterial::computeEnergy()
{
  //compute elastic energy density
  _f_el[_qp] = 0.5*_stress[_qp].doubleContraction(_elastic_strain[_qp]);

  RankTwoTensor temp;

  //compute derivative of elastic energy density
  //break the derivative into two terms, one for each Cijkl type, denoted as A and B

  //there are 3 terms in each derivative part, but the 2nd and 3rd are the same
  //doing the A term
  temp = (-1*_Cijkl_matrix*_dhdn)*_elastic_strain[_qp];
  Real first = temp.doubleContraction( _elastic_strain[_qp] );

  temp = (_Cijkl_matrix*(1 - _h))*(-1*_eigenstrain*_dhdn);
  Real second = temp.doubleContraction( _elastic_strain[_qp] );

  Real dfel_dOP_A = first + 2*second;

  //doing the B term
  temp = (_Cijkl_precip*_dhdn)*_elastic_strain[_qp];
  first = temp.doubleContraction( _elastic_strain[_qp] );

  temp = (_Cijkl_precip*(_h))*(-1*_eigenstrain*_dhdn);
  second = temp.doubleContraction( _elastic_strain[_qp] );

  Real dfel_dOP_B = first +2*second;

  _dfel_dOP[_qp] = 0.5*(dfel_dOP_A + dfel_dOP_B);



  //compute the 2nd derivative of the elastic energy density
  //it again is broken into two parts A and B as above for each type of Cijkl
  //A term
  temp = (-1*_Cijkl_matrix*_d2hdn2)*_elastic_strain[_qp];
  first = temp.doubleContraction( _elastic_strain[_qp] );

  temp = (-1*_Cijkl_matrix*_dhdn)*(-1*_eigenstrain*_dhdn);
  Real temp2 = temp.doubleContraction( _elastic_strain[_qp]);
  first += 2*temp2;

  second = temp.doubleContraction( _elastic_strain[_qp]);

  temp = (_Cijkl_matrix*(1 - _h))*(-1*_eigenstrain*_d2hdn2);
  temp2 = temp.doubleContraction( _elastic_strain[_qp] );
  second += temp2;

  temp = (_Cijkl_matrix*(1 - _h))*(-1*_eigenstrain*_dhdn);
  temp2 = temp.doubleContraction(-1*_eigenstrain*_dhdn);

  second += temp2;

  Real d2fel_dOP2_A = first + 2*second;

  //B term
  temp = (_Cijkl_precip*_d2hdn2)*_elastic_strain[_qp];
  first = temp.doubleContraction( _elastic_strain[_qp] );

  temp = (_Cijkl_precip*_dhdn)*(-1*_eigenstrain*_dhdn);
  temp2 = temp.doubleContraction( _elastic_strain[_qp]);
  first += 2*temp2;

  second = temp.doubleContraction( _elastic_strain[_qp]);

  temp = (_Cijkl_precip*(_h))*(-1*_eigenstrain*_d2hdn2);
  temp2 = temp.doubleContraction(_elastic_strain[_qp]);

  second += temp2;

  temp = (_Cijkl_precip*(_h))*(-1*_eigenstrain*_dhdn);
  temp2 = temp.doubleContraction(-1*_eigenstrain*_dhdn);

  second += temp2;

  Real d2fel_dOP2_B = first + 2*second;

  _d2fel_dOP2[_qp] = 0.5*(d2fel_dOP2_A + d2fel_dOP2_B);


  //compute elastic interaction energy
  _f_el_int[_qp] = -1*_stress[_qp].doubleContraction( _eigenstrain );
}
