# This is an input file for Hedgehog to test the interfacial energy and width
# for the CobaltTestMaterial material. It uses Cahn-Hilliard dynamics in the
# split formulation.  Units are in aJ and nm. No elasticity.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 4000
  ny = 1
  xmin = 0
  xmax = 1000
  ymin = 0
  ymax = 1

  #elem_type = QUAD4
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.01
      radius = 200
      int_width = 5
      x1 = 0
      y1 = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./interface_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./total_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./grand_potential]
     order = FIRST
     family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltBase
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    elastic_energy = 0
    variable = total_energy
  [../]

  [./aux_grand_potential]
    type = GrandPotential
    omega_eq = 0
    OP = eta1
    variable = grand_potential
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltTestMaterial #CobaltTestMaterial
    AC_mobility = 5
    well_height =  1e-1 #0.075 #4e-2 #2.1e-2 #1.95e-2 #9.7e-2 #1.95e-1
    kappa_AC =  0.29 #1.0 #2.85 #5.4e0 #5.8e-1 #1.1e0 #5.8e-1

    polynomial_order = 10
    coefficients = '230.2006355 -1151.003178 2506.663551 -3120.635139 2444.046270 -1244.129167 408.0297321 -81.24549382 8.072789087 0 0'

    order_parameter = eta1
  [../]
[]

[Postprocessors]
  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
  [./Total_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./Bulk_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = bulk_energy
  [../]

  [./Interface_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = interface_energy
  [../]

  [./Grand_Potential_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = grand_potential
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./nodes]
    type = NumNodes
  [../]
 [./numDOFs]
    type = NumDOFs
    system = NL
  [../]
#  [./VolumeFraction]
#    type = FeatureVolumeFraction
# #  bubble_volume_file = 1D_interfaceEnergy_vol.csv
#    threshold = 0.5
#    variable = eta1
#    mesh_volume = Volume
#    use_displaced_mesh = false
#  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

 [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e1
    cutback_factor = 0.25
    growth_factor = 1.05
    optimal_iterations = 5
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 600
  dtmin = 1e-2
  #dtmax = 1e2

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type  -sub_pc_type'
  petsc_options_value = 'ksp lu'

  l_tol = 1e-4
  l_max_its = 50
  nl_max_its = 10
  nl_rel_tol = 1e-8
  nl_abs_tol = 1e-10
[]


[Outputs]
  interval = 10
  csv = true
  file_base = hedgehog_testEnergy

  checkpoint = false

  [./console]
    type = Console
    perf_log = true
  [../]

  [./my_exodus]
    type = Exodus
    use_problem_dimension = false
  [../]
[]
