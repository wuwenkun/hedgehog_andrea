#This is an input file for Hackathon 2015 problem 1.

[Mesh]
  file = input/T-shape.e
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonConcIC
      epsilon = 0.01
      base_value = 0.5
      q1 = '0.105 0.11 0'
      q2 = '0.13 0.087 0'
      q3 = '0.025 0.15 0'
      q4 = '0.07 0.02 0'
      constant = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = CHSplitHackathon
    variable = c
    w = mu
    kappa_name = kappa_CH
  [../]
[]

[AuxKernels]
   [./total_energy_calc]
      type = HackathonCHEnergy
      variable = total_energy
      CH_var = c
   [../]
[]

[Materials]
  [./generic]
    type = HackathonMaterial
    CH_mobility = 5
    w = 5
    kappa_CH = 2
    c_alpha = 0.3
    c_beta = 0.7
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]

[]

[Adaptivity]
  initial_steps = 1
  max_h_level = 1
  marker = combo
  [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.5
      indicator = GJI_1
    [../]
    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.5
      indicator = GJI_2
    [../]
    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = c
    [../]
    [./GJI_2]
     type = GradientJumpIndicator
     variable = mu
    [../]
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e-1
    percent_change = 0.05
  [../]

  num_steps = 10000
  dtmin = 1e-2

  solve_type = 'PJFNK'
  #petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  #petsc_options_value = ' asm      lu           preonly       1'

  l_max_its = 100
  nl_abs_tol = 1e-11
[]


[Outputs]
  exodus = true
  interval = 10
  csv = true
  file_base = hackathon_p1_tshape
  checkpoint = true
  [./console]
    type = Console
    perf_log = true
  [../]
[]