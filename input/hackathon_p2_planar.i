#This is an input file for Hackathon 2015 problem 2.

[Mesh]
  type = GeneratedMesh
  dim = 1
  nx = 500
  ny = 0
  xmin = 0
  xmax = 100
  ymin = 0
  ymax = 100
[]

[GlobalParams]
  well_width_factor = 1.414216
  alpha = 5
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
       type = SmoothCircleIC
       radius = 20
       invalue = 0.7
       outvalue = 0.35
       int_width = 2
       x1 = 0
       y1 = 0
       z1 = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./n1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
       type = SmoothCircleIC
       radius = 20
       invalue = 1
       outvalue = 0
       int_width = 2
       x1 = 0
       y1 = 0
       z1 = 0
    [../]
  [../]

  [./n2]
    order = FIRST
    family = LAGRANGE
  [../]

  [./n3]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = CHCoupledSplitHackathon
    variable = c
    w = mu
    kappa_name = kappa_CH
    OP_var_names = 'n1 n2 n3'
   # well_width_factor = 1
  [../]

  [./ACBulk_n1]
    type = ACBulkPolyCoupledHackathon
    variable = n1
    OP_var_names = 'n1 n2 n3'
    OP_number = 1
    coupled_CH_var = c
    #well_width_factor = 1
  [../]

  [./deta1dt]
    type = TimeDerivative
    variable = n1
  [../]

  [./ACinterface1]
    type = IsotropicACInterface
    variable = n1
  [../]
#####
  [./ACBulk_n2]
    type = ACBulkPolyCoupledHackathon
    variable = n2
    OP_var_names = 'n1 n2 n3'
    OP_number = 2
    coupled_CH_var = c
    #well_width_factor = 1
  [../]

  [./deta2dt]
    type = TimeDerivative
    variable = n2
  [../]

  [./ACinterface2]
    type = IsotropicACInterface
    variable = n2
  [../]
#####
  [./ACBulk_n3]
    type = ACBulkPolyCoupledHackathon
    variable = n3
    OP_var_names = 'n1 n2 n3'
    OP_number = 3
    coupled_CH_var = c
    #well_width_factor = 1
  [../]

  [./deta3dt]
    type = TimeDerivative
    variable = n3
  [../]

  [./ACinterface3]
    type = IsotropicACInterface
    variable = n3
  [../]
[]

[AuxKernels]
  [./total_energy_calc]
    type = HackathonCHACEnergy
    variable = total_energy
    CH_var = c
    OP_var_names = 'n1 n2 n3'
    #well_width_factor = 1
   [../]
[]

[Materials]
  [./generic]
    type = HackathonMaterial
    CH_mobility = 5
    AC_mobility = 5
    w = 1
    kappa_CH = 3
    kappa_AC = 3
    c_alpha = 0.3
    c_beta = 0.7
    concentration = c
  [../]
[]

#[BCs]
#  [./Periodic]
#    [./all]
#      variable = c
#      auto_direction = 'x y'
#    [../]
#  [../]
#[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e-1
    percent_change = 0.05
  [../]

  num_steps = 10000
  dtmin = 1e-2
#  dtmax = 1e2

  solve_type = 'PJFNK'
  #petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  #petsc_options_value = ' asm      lu           preonly       1'

  l_max_its = 100
  nl_abs_tol = 1e-11
[]


[Outputs]
  exodus = true
  interval = 10
  csv = true
  file_base = hackathon_p2_planar_alpha5
  [./console]
    type = Console
    perf_log = true
  [../]
[]