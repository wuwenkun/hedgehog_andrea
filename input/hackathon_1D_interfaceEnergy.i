#This simulation is in 1D to find the interfacial energy, energy/lenth^2.
#To use this, get the equilibrium omega value first by setting omega_eq = 0 and allowing the simulation to run to equilibrium.  The omega value at either end of the simulation should be the same, and that's omega_eq.  Then run the simulation again to find the interface energy by supplying omega_eq to the auxkernel.  It outputs as gamma, the postprocessor.

[Mesh]
  type = GeneratedMesh
  dim = 1
  nx = 100
  ny = 1
  nz = 0
  xmin = 0
  xmax = 50
  ymin = 0
  ymax = 1
  zmin = 0
  zmax = 0
  elem_type = EDGE2
  #uniform_refine = 2
[]


[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
    #  type = FunctionIC
    #  function = tanhIC
      variable = c
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.1
      radius = 25
      int_width = 2
      x1 = 0
      y1 = 0
      z1 = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./omega]
    order = FIRST
    family = MONOMIAL
  [../]
[]

[AuxKernels]
  [./omega_calc]
    type = GrandPotential
    variable = omega
    OP = c
    omega_eq = 0
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = CHSplitHackathon
    variable = c
    w = mu
    kappa_name = kappa_CH
  [../]
[]

[Materials]
  [./generic]
    type = HackathonMaterial
    CH_mobility = 5
    w = 5
    kappa_CH = 2
    c_alpha = 0.3
    c_beta = 0.7
    concentration = c
  [../]
[]

[Postprocessors]
  [./dofs]
   type = NumDOFs
  [../]

  [./Gamma]
    type = ElementIntegralVariablePostprocessor
    variable = omega
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1
    percent_change = 0.05
  [../]

  #Preconditioned JFNK (default)
  solve_type = 'PJFNK'

  l_max_its = 100
  nl_abs_tol = 1e-12
  ss_check_tol = 1e-8
  trans_ss_check = 1

  num_steps = 5000

  dtmin = 1e-2
  dtmax = 1E2
[]

#[Adaptivity]
#  marker = EFM_1
#  initial_steps = 3
#  max_h_level = 3
#  [./Markers]
#    [./EFM_1]
#      type = ErrorFractionMarker
#      coarsen = 0.075
#      refine = 0.75
#      indicator = GJI_1
#    [../]
#  [../]

#  [./Indicators]
#    [./GJI_1]
#     type = GradientJumpIndicator
#      variable = eta1
#    [../]
#  [../]
#[]

[Outputs]
  file_base = hacakthon_1D_interfaceEnergy

  exodus = true
  interval = 5
  checkpoint = 0
  csv = true

  [./console]
    type = Console
    interval = 5
    max_rows = 10
  [../]
[]
