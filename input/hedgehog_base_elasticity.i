#This is a prototypical Hedgehog input file.  It uses Cahn-Hilliard dynamics in the split formulation.  Units are in aJ and nm.  Elasticity is included; the material is modeled as linear elastic.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 12
  ny = 12
  xmin = 0
  xmax = 100
  ymin = 0
  ymax = 100

  elem_type = QUAD4
[]

[MeshModifiers]
  [./AddExtraNodeset]
    #this is done to fix a node for mechanical computations
    new_boundary = 100
    coord = '0 0'
    type = AddExtraNodeset
  [../]
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothCircleIC
      invalue = 1
      outvalue = 0.05
      radius = 20
      int_width = 2
      x1 = 50
      y1 = 50
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_x]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_y]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./elastic_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./interface_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./s11_aux]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./s12_aux]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./s22_aux]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./e11_aux]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./e12_aux]
    order = CONSTANT
    family = MONOMIAL
  [../]

  [./e22_aux]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./TensorMechanics]
    displacements = 'disp_x disp_y'
  [../]

  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltElasticity
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_elastic_energy]
    type = ElasticEnergy
    variable = elastic_energy
  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    elastic_energy = elastic_energy
    variable = total_energy
  [../]

  [./matl_s11]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 0
    variable = s11_aux
  [../]

 [./matl_s12]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 1
    variable = s12_aux
  [../]

  [./matl_s22]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 1
    index_j = 1
    variable = s22_aux
  [../]

  [./matl_e11]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 0
    variable = e11_aux
  [../]

  [./matl_e12]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 1
    variable = e12_aux
  [../]

  [./matl_e22]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 1
    index_j = 1
    variable = e22_aux
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltBaseMaterial
    AC_mobility = 1
    well_height = 7.2e-2
    kappa_AC = 2.25e-1

    order_parameter = eta1
  [../]

  [./gamma_gammaprime]
    type = HeterogeneousLinearElasticMaterial

    #reading C_11 C_12 C_13 C_22 C_23 C_33 C_44 C_55 C_66
    #units of aJ/nm^3
    Cijkl_matrix = '250 150 150 250 150 250 130 130 130'
    Cijkl_precip = '271 172 172 271 172 271 162 162 162'
    precipitate_eigenstrain = '0.005 0.005 0.005 0 0 0'

    order_parameter = eta1
    disp_x = disp_x
    disp_y = disp_y
  [../]
[]

[BCs]
 [./pin_nodex]
    type = DirichletBC
    variable = disp_x
    value = 0.0
    boundary = '100'
  [../]

 [./pin_nodey]
    type = DirichletBC
    variable = disp_y
    value = 0.0
    boundary = '100'
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]

  [./SystemFreeEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]

  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]

  [./VolumeFraction]
    type = NodalVolumeFraction
 #  bubble_volume_file = 1D_interfaceEnergy_vol.csv
    threshold = 0.5
    variable = eta1
    mesh_volume = Volume
  [../]
[]

[Adaptivity]
  marker = combo
  initial_steps = 3
  max_h_level = 3
  [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.75
      indicator = GJI_1
    [../]

    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.75
      indicator = GJI_2
    [../]

    [./EFM_3]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.75
      indicator = GJI_3
    [../]

    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2 EFM_3'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = eta1
    [../]

    [./GJI_2]
     type = GradientJumpIndicator
     variable = disp_x
    [../]

    [./GJI_3]
     type = GradientJumpIndicator
     variable = disp_y
    [../]
  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e1
    percent_change = 0.05
  [../]

  num_steps = 20
  dtmin = 1e-2
  dtmax = 1e2

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -sub_pc_type'
  petsc_options_value = ' asm      lu         '

  l_max_its = 100
  nl_rel_tol = 1e-6
  nl_abs_tol = 1e-12
[]

[Outputs]
  console = true
  csv = true
  print_perf_log = true
  print_linear_residuals = 1

  [./out]
    type = Exodus
    interval = 1
    file_base = hedgehog_base_elasticity
    output_material_properties = true
  [../]
[]
