#This is an input file for Hackathon 2015 problem 2.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 13
  ny = 13
  xmin = 50
  xmax = 75
  ymin = 50
  ymax = 75

 elem_type = QUAD4
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
       type = SmoothCircleIC
       radius = 8
       invalue = 0.5
       outvalue = 0.4
       int_width = 2
       x1 = 50
       y1 = 50
       z1 = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./n1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
       type = SmoothCircleIC
       radius = 8
       invalue = 1
       outvalue = 0
       int_width = 2
       x1 = 50
       y1 = 50
       z1 = 0
    [../]
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = CHCoupledSplitHackathon
    variable = c
    w = mu
    kappa_name = kappa_CH
    OP_var_names = 'n1'
    well_width_factor = 1
  [../]

  [./ACBulk_n1]
    type = ACBulkPolyCoupledHackathon
    variable = n1
    OP_var_names = 'n1'
    OP_number = 1
    coupled_CH_var = c
    well_width_factor = 1
  [../]

  [./deta1dt]
    type = TimeDerivative
    variable = n1
  [../]

  [./ACinterface1]
    type = IsotropicACInterface
    variable = n1
  [../]
[]

[AuxKernels]
  [./total_energy_calc]
    type = HackathonCHACEnergy
    variable = total_energy
    CH_var = c
    OP_var_names = 'n1'
    well_width_factor = 1
   [../]
[]

[Materials]
  [./generic]
    type = HackathonMaterial
    CH_mobility = 5
    AC_mobility = 0.5
    w = 3
    kappa_CH = 2
    kappa_AC = 2
    c_alpha = 0.3
    c_beta = 0.5
  [../]
[]

#[BCs]
#  [./Periodic]
#    [./all]
#      variable = c
#      auto_direction = 'x y'
#    [../]
#  [../]
#[]

[Adaptivity]
  initial_steps = 3

  max_h_level = 3
  marker = combo
 [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.05
      indicator = GJI_1
    [../]
    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.05
      indicator = GJI_2
    [../]
    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = c
    [../]
   [./GJI_2]
     type = GradientJumpIndicator
     variable = mu
    [../]
  [../]
[]


[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e-1
    percent_change = 0.05
  [../]

  num_steps = 10000
  dtmin = 1e-2
#  dtmax = 1e2

  solve_type = 'PJFNK'
  #petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  #petsc_options_value = ' asm      lu           preonly       1'

  l_max_its = 100
  nl_abs_tol = 1e-11
[]


[Outputs]
  exodus = true
  interval = 10
  csv = true
  file_base = hackathon_p2_GT1
  [./console]
    type = Console
    perf_log = true
  [../]
[]