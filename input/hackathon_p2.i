#This is an input file for Hackathon 2015 problem 2.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 32
  ny = 32
  xmin = 0
  xmax = 200
  ymin = 0
  ymax = 200

  elem_type = QUAD4
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonConcIC
      epsilon = 0.05
      base_value = 0.5
      q1 = '0.105 0.11 0'
      q2 = '0.13 0.087 0'
      q3 = '0.025 0.15 0'
      q4 = '0.07 0.02 0'
      constant = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./n1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.01 0.017 0'
      q2 = '0.12 0.12 0'
      q3 = '0.047 0.0415 0'
      q4 = '0.032 0.005 0'
      h = 1.5
    [../]
  [../]

  [./n2]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.02 0.027 0'
      q2 = '0.13 0.13 0'
      q3 = '0.048 0.0425 0'
      q4 = '0.033 0.006 0'
      h = 1.5
    [../]
  [../]

  [./n3]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.03 0.037 0'
      q2 = '0.14 0.14 0'
      q3 = '0.049 0.0435 0'
      q4 = '0.034 0.007 0'
      h = 1.5
    [../]
  [../]

  [./n4]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.04 0.047 0'
      q2 = '0.15 0.15 0'
      q3 = '0.05 0.0445 0'
      q4 = '0.035 0.008 0'
      h = 1.5
    [../]
  [../]

  [./n5]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.05 0.057 0'
      q2 = '0.16 0.16 0'
      q3 = '0.051 0.0455 0'
      q4 = '0.036 0.009 0'
      h = 1.5
    [../]
  [../]

  [./n6]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.06 0.067 0'
      q2 = '0.17 0.17 0'
      q3 = '0.052 0.0465 0'
      q4 = '0.037 0.01 0'
      h = 1.5
    [../]
  [../]

  [./n7]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.07 0.077 0'
      q2 = '0.18 0.18 0'
      q3 = '0.053 0.0475 0'
      q4 = '0.038 0.0011 0'
      h = 1.5
    [../]
  [../]

  [./n8]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.08 0.087 0'
      q2 = '0.19 0.19 0'
      q3 = '0.054 0.0485 0'
      q4 = '0.039 0.012 0'
      h = 1.5
    [../]
  [../]

  [./n9]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.09 0.097 0'
      q2 = '0.2 0.2 0'
      q3 = '0.055 0.0495 0'
      q4 = '0.04 0.013 0'
      h = 1.5
    [../]
  [../]

  [./n10]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = HackathonEtaIC
      epsilon = 0.1
      q1 = '0.1 0.107 0'
      q2 = '0.21 0.21 0'
      q3 = '0.056 0.0505 0'
      q4 = '0.041 0.014 0'
      h = 1.5
    [../]
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = CHCoupledSplitHackathon
    variable = c
    w = mu
    kappa_name = kappa_CH
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    well_width_factor = 2
  [../]

  [./ACBulk_n1]
    type = ACBulkPolyCoupledHackathon
    variable = n1
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 1
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta1dt]
    type = TimeDerivative
    variable = n1
  [../]

  [./ACinterface1]
    type = IsotropicACInterface
    variable = n1
  [../]
#####
  [./ACBulk_n2]
    type = ACBulkPolyCoupledHackathon
    variable = n2
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 2
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta2dt]
    type = TimeDerivative
    variable = n2
  [../]

  [./ACinterface2]
    type = IsotropicACInterface
    variable = n2
  [../]
#####
  [./ACBulk_n3]
    type = ACBulkPolyCoupledHackathon
    variable = n3
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 3
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta3dt]
    type = TimeDerivative
    variable = n3
  [../]

  [./ACinterface3]
    type = IsotropicACInterface
    variable = n3
  [../]
#####
  [./ACBulk_n4]
    type = ACBulkPolyCoupledHackathon
    variable = n4
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 4
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta4dt]
    type = TimeDerivative
    variable = n4
  [../]

  [./ACinterface4]
    type = IsotropicACInterface
    variable = n4
  [../]
#####
  [./ACBulk_n5]
    type = ACBulkPolyCoupledHackathon
    variable = n5
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 5
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta5dt]
    type = TimeDerivative
    variable = n5
  [../]

  [./ACinterface5]
    type = IsotropicACInterface
    variable = n5
  [../]
#####
  [./ACBulk_n6]
    type = ACBulkPolyCoupledHackathon
    variable = n6
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 6
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta6dt]
    type = TimeDerivative
    variable = n6
  [../]

  [./ACinterface6]
    type = IsotropicACInterface
    variable = n6
  [../]
#####
  [./ACBulk_n7]
    type = ACBulkPolyCoupledHackathon
    variable = n7
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 7
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta7dt]
    type = TimeDerivative
    variable = n7
  [../]

  [./ACinterface7]
    type = IsotropicACInterface
    variable = n7
  [../]
#####
  [./ACBulk_n8]
    type = ACBulkPolyCoupledHackathon
    variable = n8
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 8
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta8dt]
    type = TimeDerivative
    variable = n8
  [../]

  [./ACinterface8]
    type = IsotropicACInterface
    variable = n8
  [../]
#####
  [./ACBulk_n9]
    type = ACBulkPolyCoupledHackathon
    variable = n9
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 9
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta9dt]
    type = TimeDerivative
    variable = n9
  [../]

  [./ACinterface9]
    type = IsotropicACInterface
    variable = n9
  [../]
#####
  [./ACBulk_n10]
    type = ACBulkPolyCoupledHackathon
    variable = n10
    OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
    OP_number = 10
    coupled_CH_var = c
    well_width_factor = 2
  [../]

  [./deta10dt]
    type = TimeDerivative
    variable = n10
  [../]

  [./ACinterface10]
    type = IsotropicACInterface
    variable = n10
  [../]
[]

[AuxKernels]
   [./total_energy_calc]
      type = HackathonCHACEnergy
      variable = total_energy
      CH_var = c
      OP_var_names = 'n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
      well_width_factor = 2
   [../]
[]

[Materials]
  [./generic]
    type = HackathonMaterial
    CH_mobility = 5
    AC_mobility = 5
    w = 5
    kappa_CH = 2
    kappa_AC = 2
    c_alpha = 0.3
    c_beta = 0.7
  [../]
[]

[BCs]
  [./Periodic]
    [./all]
      variable = 'c n1 n2 n3 n4 n5 n6 n7 n8 n9 n10'
      auto_direction = 'x y'
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
[]

[Adaptivity]
  initial_steps = 4
  max_h_level = 4
  marker = combo
 [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.5
      indicator = GJI_1
    [../]
    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.5
      indicator = GJI_2
    [../]
    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = c
    [../]
   [./GJI_2]
     type = GradientJumpIndicator
     variable = mu
    [../]
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e-2
    percent_change = 0.05
  [../]

  num_steps = 10000
  dtmin = 1e-2

  solve_type = 'PJFNK'
  #petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  #petsc_options_value = ' asm      lu           preonly       1'

  l_max_its = 100
  nl_max_its = 10
  nl_abs_tol = 1e-11
[]


[Outputs]
  exodus = true
  interval = 10
  csv = true
  file_base = hackathon_p2_square_PBC
  checkpoint = true
  [./console]
    type = Console
    perf_log = true
  [../]
[]
