#This is an input file for the Elasticity benchmark problem. 2D simulation. This file is for a system with homogeneous modulus between the phases and an initial particle radius of 25 nm.
[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 46
  ny = 46
  nz = 0
  xmin = -750
  xmax = 750
  ymin = -750
  ymax = 750
  zmin = 0
  zmax = 0

  elem_type = QUAD4
[]

[MeshModifiers]
  [./x_mirror]
    type = AddExtraNodeset
    new_boundary = 'x_mirror'
    coord = '0 0  0 750  0 -750'
  [../]
  [./y_mirror]
    type = AddExtraNodeset
    new_boundary = 'y_mirror'
    coord = '0 0  -750 0  750 0'
  [../]
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = SmoothSuperellipsoidIC
      invalue = 1
      outvalue = 0.005
      a = 75
      b = 75
      c = 75
      n = 2
      int_width = 5
      x1 = 0
      y1 = 0
      z1 = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_x]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_y]
    order = FIRST
    family = LAGRANGE
  [../]

#  [./disp_z]
#    order = FIRST
#    family = LAGRANGE
#  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./elastic_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./grad_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./total_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s11_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s12_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s22_aux]
    order = FIRST
    family = MONOMIAL
  [../]

 # [./s23_aux]
 #   order = FIRST
 #   family = MONOMIAL
 # [../]

 # [./s13_aux]
 #   order = FIRST
 #   family = MONOMIAL
 # [../]

 # [./s33_aux]
 #   order = FIRST
 #   family = MONOMIAL
 # [../]

  [./e11_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e12_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e22_aux]
    order = FIRST
    family = MONOMIAL
  [../]

 # [./e23_aux]
 #   order = FIRST
 #   family = MONOMIAL
 # [../]

 # [./e13_aux]
 #   order = FIRST
 #   family = MONOMIAL
 # [../]

 # [./e33_aux]
 #   order = FIRST
 #   family = MONOMIAL
 # [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./TensorMechanics]
    displacements = 'disp_x disp_y' # disp_z'
  [../]

  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltElasticity
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_elastic_energy]
    type = ElasticEnergy
    variable = elastic_energy
  [../]

  [./aux_grad_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = grad_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = grad_energy
    elastic_energy = elastic_energy
    variable = total_energy
  [../]

  [./matl_s11]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 0
    variable = s11_aux
  [../]

 [./matl_s12]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 1
    variable = s12_aux
  [../]

  [./matl_s22]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 1
    index_j = 1
    variable = s22_aux
  [../]

#  [./matl_s13]
#    type = RankTwoAux
#    rank_two_tensor = stress
#    index_i = 0
#    index_j = 2
#    variable = s13_aux
#  [../]

# [./matl_s23]
#    type = RankTwoAux
#    rank_two_tensor = stress
#    index_i = 1
#    index_j = 2
#    variable = s23_aux
#  [../]

#  [./matl_s33]
#    type = RankTwoAux
#    rank_two_tensor = stress
#    index_i = 2
#    index_j = 2
#    variable = s33_aux
#  [../]

  [./matl_e11]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 0
    variable = e11_aux
  [../]

  [./matl_e12]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 1
    variable = e12_aux
  [../]

  [./matl_e22]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 1
    index_j = 1
    variable = e22_aux
  [../]

# [./matl_e13]
#    type = RankTwoAux
#    rank_two_tensor = elastic_strain
#    index_i = 0
#    index_j = 2
#    variable = e13_aux
#  [../]

#  [./matl_e23]
#    type = RankTwoAux
#    rank_two_tensor = elastic_strain
#    index_i = 1
#    index_j = 2
#    variable = e23_aux
#  [../]

#  [./matl_e33]
#    type = RankTwoAux
#    rank_two_tensor = elastic_strain
#    index_i = 2
#    index_j = 2
#    variable = e33_aux
#  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltTestMaterial
    AC_mobility = 5
    well_height = 0.1
    kappa_AC = 0.29

    polynomial_order = 10
    coefficients = '230.2006355 -1151.003178 2506.663551 -3120.635139 2444.046270 -1244.129167 408.0297321 -81.24549382 8.072789087 0 0'

    order_parameter = eta1
  [../]

  [./gamma_gammaprime]

    type = HeterogeneousLinearElasticMaterial
    #reading C_11 C_12 C_13 C_22 C_23 C_33 C_44 C_55 C_66
    #units of aJ/nm^3
    Cijkl_matrix = '250 150 150 250 150 250 100 100 100'
    Cijkl_precip = '275 165 165 275 165 275 110 110 110'
    precipitate_eigenstrain = '0.005 0.005 0.005 0 0 0'

    order_parameter = eta1
    disp_x = disp_x
    disp_y = disp_y
 #   disp_z = disp_z
  [../]
[]

[BCs]
 [./pin_nodex]
    type = DirichletBC
    variable = disp_x
    value = 0.0
    boundary = 'x_mirror'
  [../]

 [./pin_nodey]
    type = DirichletBC
    variable = disp_y
    value = 0.0
    boundary = 'y_mirror'
 [../]

# [./pin_nodez]
#    type = DirichletBC
#    variable = disp_z
#    value = 0.0
#    boundary = 'back'
#  [../]

#  [./Stress_dispx]
#    type = StressBC
#    variable = disp_x
#    component = 0
#    boundary_stress = '0 0 0.4 0 0 0'
#    boundary = 'front'
#  [../]

#  [./Stress_dispy]
#    type = StressBC
#    variable = disp_y
#    component = 1
#    boundary_stress = '0 0 0.4 0 0 0'
#    boundary = 'front'
#  [../]

#  [./Stress_dispz]
#    type = StressBC
#    variable = disp_z
#    component = 2
#    boundary_stress = '0 0 0.4 0 0 0'
#    boundary = 'front'
#  [../]
[]


[Postprocessors]
  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
  [./Total_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./Bulk_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = bulk_energy
  [../]

  [./Interface_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = grad_energy
  [../]

  [./Elastic_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = elastic_energy
  [../]

  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
 [./numDOFs]
    type = NumDOFs
    system = NL
  [../]
  [./Volume_Fraction]
    type = FeatureVolumeFraction
    mesh_volume = Volume
    feature_volumes = feature_volumes
    execute_on = 'initial timestep_end'
  [../]
   [./feature_counter]
    type = FeatureFloodCount
    variable = eta1
    compute_var_to_feature_map = true
    execute_on = 'initial timestep_end'
  [../]
[]

[VectorPostprocessors]
  [./feature_volumes]
    type = FeatureVolumeVectorPostprocessor
    flood_counter = feature_counter
    execute_on = 'initial timestep_end'
    outputs = none
  [../]
[]

[Adaptivity]
  marker = combo
  initial_steps = 5
  max_h_level = 5
  [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.75
      indicator = GJI_1
    [../]

    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.5
      indicator = GJI_2
    [../]

    [./EFM_3]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.5
      indicator = GJI_3
    [../]

 #   [./EFM_4]
 #     type = ErrorFractionMarker
 #     coarsen = 0.05
 #     refine = 0.25
 #     indicator = GJI_4
 #   [../]

    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2 EFM_3' #EFM_4'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = eta1
    [../]

    [./GJI_2]
     type = GradientJumpIndicator
     variable = disp_x
    [../]

    [./GJI_3]
     type = GradientJumpIndicator
     variable = disp_y
    [../]

#    [./GJI_4]
#     type = GradientJumpIndicator
#     variable = disp_z
#    [../]
  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

 [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e-1
    cutback_factor = 0.75
    growth_factor = 1.05
    optimal_iterations = 6
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 5000
  dtmin = 1e-2
  #dtmax = 1e2

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type  -sub_pc_type'
  petsc_options_value = 'ksp lu'

  l_tol = 1e-4
  l_max_its = 50
  nl_max_its = 10

  nl_rel_tol = 1e-6
  nl_abs_tol = 1e-9
[]


[Outputs]
  interval = 10
  csv = true
  file_base = r75_heterogeneous
  checkpoint = true

  [./console]
    type = Console
    perf_log = true
  [../]

  [./my_exodus]
    type = Exodus
    #use_problem_dimension = false
  [../]
[]
