#This is an input file for Hackathon 2015 problem 1.

[Mesh]
  type = GeneratedMesh
  dim = 1
  nx = 500
  ny = 0
  xmin = 0
  xmax = 100
  ymin = 0
  ymax = 100
[]

[Variables]
  [./c]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
       type = SmoothCircleIC
       radius = 20
       invalue = 0.7
       outvalue = 0.35
       int_width = 2
       x1 = 0
       y1 = 0
       z1 = 0
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[AuxVariables]
  [./total_energy]
    order = CONSTANT
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = CH_mobility
  [../]

  [./dcdt]
    type = CoupledTimeDerivative
    variable = mu
    v = c
  [../]

  [./c_residual]
    type = CHSplitHackathon
    variable = c
    w = mu
    kappa_name = kappa_CH
  [../]
[]

[AuxKernels]
   [./total_energy_calc]
      type = HackathonCHEnergy
      variable = total_energy
      CH_var = c
   [../]
[]

[Materials]
  [./generic]
    type = HackathonMaterial
    CH_mobility = 5
    w = 5
    kappa_CH = 2
    c_alpha = 0.3
    c_beta = 0.7
    concentration = c
  [../]
[]

[Postprocessors]
  [./numDOFs]
    type = NumDOFs
  [../]
  [./TotalEnergy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
  [./L_evals]
    type = NumResidualEvaluations
  [../]
  [./nodes]
    type = NumNodes
  [../]
[]

[Executioner]
  type = Transient

  [./TimeStepper]
    type = SolutionTimeAdaptiveDT
    dt = 1e-1
    percent_change = 0.05
  [../]

  num_steps = 1000
  dtmin = 1e-2

  solve_type = 'PJFNK'
  #petsc_options_iname = '-pc_type -sub_pc_type -sub_ksp_type -pc_asm_overlap'
  #petsc_options_value = ' asm      lu           preonly       1'

  l_max_its = 100
  nl_abs_tol = 1e-11
[]


[Outputs]
  exodus = true
  interval = 10
  csv = true
  file_base = hackathon_p1_planar
  [./console]
    type = Console
    perf_log = true
  [../]
[]
