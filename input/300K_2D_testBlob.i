#This is an input file for Hedgehog to mirror boundary conditions for systems with applied
#stresses.  It is 2D.  It uses Cahn-Hilliard dynamics in the split formulation.
#Units are in aJ and nm. The material is modeled as linear elastic.

[Mesh]
  type = GeneratedMesh
  dim = 2
  nx = 20
  ny = 20
  nz = 20
  xmin = -1000
  xmax = 1000
  ymin = -1000
  ymax = 1000
  zmin = -1000
  zmax = 1000

  #uniform_refine = 3
[]

[MeshModifiers]
  [./AddExtraNodeSet]
    new_boundary = 100
    coord = '-1000 0  0 0  1000 0'
    #coord = '-1000 -1000'
    type = AddExtraNodeset
  []
[]

[Variables]
  [./eta1]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = BlobIC
      invalue = 1
      outvalue = 0.01
      int_width = 20
      a = 117
      b = 110
#      int_width = 10
    [../]
  [../]

  [./mu]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_x]
    order = FIRST
    family = LAGRANGE
  [../]

  [./disp_y]
    order = FIRST
    family = LAGRANGE
  [../]

#  [./disp_z]
#    order = FIRST
#    family = LAGRANGE
#  [../]
[]

[AuxVariables]
  [./bulk_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./elastic_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./interface_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./total_energy]
    order = FIRST
    family = MONOMIAL
  [../]

   [./elastic_interaction_energy]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s11_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s12_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s22_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s23_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s13_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./s33_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e11_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e12_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e22_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e23_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e13_aux]
    order = FIRST
    family = MONOMIAL
  [../]

  [./e33_aux]
    order = FIRST
    family = MONOMIAL
  [../]
[]

[Preconditioning]
  [./SMP]
    type = SMP
    full = true
  [../]
[]

[Kernels]
  [./TensorMechanics]
    displacements = 'disp_x disp_y' #disp_z'
  [../]

  [./mu_residual]
    type = SplitCHWRes
    variable = mu
    mob_name = AC_mobility
  [../]

  [./detadt]
    type = CoupledTimeDerivative
    variable = mu
    v = eta1
  [../]

  [./eta_residual]
    type = SplitCHCobaltElasticity
    variable = eta1
    w = mu
    kappa_name = kappa_AC
  [../]
[]

[AuxKernels]
  [./aux_bulk_energy]
    type = BulkFreeEnergy
    variable = bulk_energy
  [../]

  [./aux_elastic_energy]
    type = ElasticEnergy
    variable = elastic_energy
  [../]

  [./aux_int_energy]
    type = InterfaceFreeEnergy
    order_parameter = eta1
    variable = interface_energy
  [../]

  [./aux_total_energy]
    type = FreeEnergyTotal
    bulk_energy = bulk_energy
    interface_energy = interface_energy
    elastic_energy = elastic_energy
    variable = total_energy
  [../]

  [./aux_elastic_interaction_energy]
    type = ElasticInteractionEnergy
    variable = elastic_interaction_energy
  [../]

  [./matl_s11]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 0
    variable = s11_aux
  [../]

 [./matl_s12]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 1
    variable = s12_aux
  [../]

  [./matl_s22]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 1
    index_j = 1
    variable = s22_aux
  [../]

  [./matl_s13]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 0
    index_j = 2
    variable = s13_aux
  [../]

 [./matl_s23]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 1
    index_j = 2
    variable = s23_aux
  [../]

  [./matl_s33]
    type = RankTwoAux
    rank_two_tensor = stress
    index_i = 2
    index_j = 2
    variable = s33_aux
  [../]

  [./matl_e11]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 0
    variable = e11_aux
  [../]

  [./matl_e12]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 1
    variable = e12_aux
  [../]

  [./matl_e22]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 1
    index_j = 1
    variable = e22_aux
  [../]

 [./matl_e13]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 0
    index_j = 2
    variable = e13_aux
  [../]

  [./matl_e23]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 1
    index_j = 2
    variable = e23_aux
  [../]

  [./matl_e33]
    type = RankTwoAux
    rank_two_tensor = elastic_strain
    index_i = 2
    index_j = 2
    variable = e33_aux
  [../]
[]

[Materials]
  [./cobaltish]
    type = CobaltTestMaterial #CobaltTestMaterial
    AC_mobility = 5
    well_height = 1.0e-1
    kappa_AC = 1.14

    polynomial_order = 10
    coefficients = '230.2006355 -1151.003178 2506.663551 -3120.635139 2444.046270 -1244.129167 408.0297321 -81.24549382 8.072789087 0 0'

    order_parameter = eta1
  [../]

  [./gamma_gammaprime]

    type = HeterogeneousLinearElasticMaterial
    #reading C_11 C_12 C_13 C_22 C_23 C_33 C_44 C_55 C_66
    #units of aJ/nm^3
    Cijkl_matrix = '229 165 165 229 165 229 97.3 97.3 97.3'
    Cijkl_precip = '272 158 158 272 158 272 162 162 162'
    precipitate_eigenstrain = '0.0053 0.0053 0.0053 0 0 0'

    order_parameter = eta1
    disp_x = disp_x
    disp_y = disp_y
    #disp_z = disp_z
  [../]
[]

[BCs]
 #[./pin_nodex]
 #   type = DirichletBC
 #   variable = disp_x
 #   value = 0.0
 #   boundary = '100'
 # [../]

 [./pin_nodey_top]
    type = DirichletBC
    variable = disp_y
    value = 0
    boundary = '100'
  [../]


# [./pin_nodez]
#    type = DirichletBC
#    variable = disp_z
#    value = 0.0
#    boundary = 'back'
#  [../]

  [./Stress_dispx]
    type = StressBC
    variable = disp_x
    component = 0
    boundary_stress = '0 -0.4 0 0 0 0'
    boundary = 'top bottom'
  [../]

  [./Stress_dispy]
    type = StressBC
    variable = disp_y
    component = 1
    boundary_stress = '0 -0.4 0 0 0 0'
    boundary = 'top bottom'
  [../]

#  [./Stress_dispz]
#    type = StressBC
#    variable = disp_z
#    component = 2
#    boundary_stress = '0 0 0.4 0 0 0'
#    boundary = 'front'
#  [../]
[]


[Postprocessors]
  [./Volume]
    type = VolumePostprocessor
    execute_on = initial
  [../]
  [./Total_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = total_energy
  [../]
  [./Bulk_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = bulk_energy
  [../]

  [./Interface_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = interface_energy
  [../]

  [./Elastic_Energy]
    type = ElementIntegralVariablePostprocessor
    variable = elastic_energy
  [../]

  [./dt]
    type = TimestepSize
  [../]
  [./NL_iter]
    type = NumNonlinearIterations
  [../]
 [./numDOFs]
    type = NumDOFs
    system = NL
  [../]
  [./VolumeFraction]
    type = NodalVolumeFraction
 #  bubble_volume_file = 1D_interfaceEnergy_vol.csv
    threshold = 0.5
    variable = eta1
    mesh_volume = Volume
  [../]
[]

[Adaptivity]
  marker = combo
  initial_steps = 6
  #initial_marker = EFM_1
  max_h_level = 6
  [./Markers]
    [./EFM_1]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.75
      indicator = GJI_1
    [../]

    [./EFM_2]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.25
      indicator = GJI_2
    [../]

    [./EFM_3]
      type = ErrorFractionMarker
      coarsen = 0.05
      refine = 0.25
      indicator = GJI_3
    [../]

#    [./EFM_4]
#      type = ErrorFractionMarker
#      coarsen = 0.05
#      refine = 0.25
#      indicator = GJI_4
#    [../]

    [./combo]
      type = ComboMarker
      markers = 'EFM_1 EFM_2 EFM_3' # EFM_4'
    [../]
  [../]

  [./Indicators]
    [./GJI_1]
     type = GradientJumpIndicator
     variable = eta1
    [../]

    [./GJI_2]
     type = GradientJumpIndicator
     variable = disp_x
    [../]

    [./GJI_3]
     type = GradientJumpIndicator
     variable = disp_y
    [../]

#    [./GJI_4]
#     type = GradientJumpIndicator
#     variable = disp_z
#    [../]
  [../]
[]

[Executioner]
  type = Transient
  scheme = 'BDF2'

 [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 1e-1
    cutback_factor = 0.75
    growth_factor = 1.05
    optimal_iterations = 6
    iteration_window = 1
    linear_iteration_ratio = 100
  [../]

  num_steps = 2
  dtmin = 1e-2
  #dtmax = 1e2

  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type  -sub_pc_type'
  petsc_options_value = 'ksp lu'

  l_tol = 1e-4
  l_max_its = 50
  nl_max_its = 10
  nl_rel_tol = 1e-6
  nl_abs_tol = 2e-9
[]


[Outputs]
  interval = 1
  csv = true
  file_base = r100_2D_300K_m400MPa_testBlob
  checkpoint = true

  [./console]
    type = Console
    perf_log = true
  [../]

  [./my_exodus]
    type = Exodus
    #use_problem_dimension = false
  [../]
[]
