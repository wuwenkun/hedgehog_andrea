/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   4 February 2016
 *
 *****************************************************/

#ifndef FREEENERGYTOTAL_H
#define FREEENERGYTOTAL_H

#include "AuxKernel.h"

//forward declarations
class FreeEnergyTotal;

template<>
InputParameters validParams<FreeEnergyTotal>();

class FreeEnergyTotal : public AuxKernel
{
public:
  FreeEnergyTotal(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const VariableValue & _fchem;
  const VariableValue & _int_energy;
  const VariableValue & _elastic_energy;

};

#endif //FREEENERGYTOTAL_H
