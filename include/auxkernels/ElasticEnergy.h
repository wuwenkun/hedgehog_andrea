/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   11 February 2016
 *
 *****************************************************/

#ifndef ELASTICENERGY_H
#define ELASTICENERGY_H

#include "AuxKernel.h"

//forward declarations
class ElasticEnergy;

template<>
InputParameters validParams<ElasticEnergy>();

class ElasticEnergy : public AuxKernel
{
public:
  ElasticEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const MaterialProperty<Real> & _felastic;

};

#endif //ELASTICENERGY_H
