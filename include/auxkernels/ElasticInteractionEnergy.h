/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   20 April 2016
 *
 *****************************************************/

#ifndef ELASTICINTERACTIONENERGY_H
#define ELASTICINTERACTIONENERGY_H

#include "AuxKernel.h"

//forward declarations
class ElasticInteractionEnergy;

template<>
InputParameters validParams<ElasticInteractionEnergy>();

class ElasticInteractionEnergy : public AuxKernel
{
public:
  ElasticInteractionEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const MaterialProperty<Real> & _felasticInt;

};

#endif //ELASTICINTERACTIONENERGY_H
