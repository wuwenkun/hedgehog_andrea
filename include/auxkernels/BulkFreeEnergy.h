/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   4 February 2016
 *
 *****************************************************/

#ifndef BULKFREEENERGY_H
#define BULKFREEENERGY_H

#include "AuxKernel.h"

//forward declarations
class BulkFreeEnergy;

template<>
InputParameters validParams<BulkFreeEnergy>();

class BulkFreeEnergy : public AuxKernel
{
public:
  BulkFreeEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const MaterialProperty<Real> & _fbulk;

};

#endif //BULKFREEENERGY_H
