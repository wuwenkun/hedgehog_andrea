/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   9 February 2016
 *
 *****************************************************/

#ifndef GRANDPOTENTIAL_H
#define GRANDPOTENTIAL_H

#include "AuxKernel.h"

class GrandPotential;


template<>
InputParameters validParams<GrandPotential>();

class GrandPotential : public AuxKernel
{
public:
  GrandPotential(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:
  const VariableValue & _OP;
  const VariableGradient & _grad_OP;
  const VariableSecond & _second_OP;

  Real _omega_eq;

  const MaterialProperty<Real> & _fbulk;
  const MaterialProperty<Real> & _dfbulk_dOP;
  const MaterialProperty<Real> & _kappa;

};

#endif //GRANDPOTENTIAL_H
