/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   4 February 2016
 *
 *****************************************************/

#ifndef INTERFACEFREEENERGY_H
#define INTERFACEFREEENERGY_H

#include "AuxKernel.h"

//forward declarations
class InterfaceFreeEnergy;

template<>
InputParameters validParams<InterfaceFreeEnergy>();

class InterfaceFreeEnergy : public AuxKernel
{
public:
  InterfaceFreeEnergy(const InputParameters & parameters);

protected:
  virtual Real computeValue();

private:

  const VariableGradient & _grad_OP;
  const MaterialProperty<Real> & _kappa;
};

#endif //INTERFACEFREEENERGY_H
