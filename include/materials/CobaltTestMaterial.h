/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   31 May 2016
 *
 *****************************************************/

#ifndef COBALTTESTMATERIAL_H
#define COBALTTESTMATERIAL_H

#include "CobaltBaseMaterial.h"

//forward declarations
class CobaltTestMaterial;

template<>
InputParameters validParams<CobaltTestMaterial>();

class CobaltTestMaterial : public CobaltBaseMaterial
{
public:
  CobaltTestMaterial(const InputParameters & parameters);

protected:
  virtual void computeQpProperties();

  //MaterialProperty<Real> & _L;
  //MaterialProperty<Real> & _kappa_AC;

  //MaterialProperty<Real> & _fbulk;
  //MaterialProperty<Real> & _dfbulkdOP;
  //MaterialProperty<Real> & _d2fbulkdOP2;

  //these store the values from the input file.
  //Real _L_param;
  //Real _w_param;
  //Real _kappa_AC_param;

  //const VariableValue & _OP;
  unsigned int _polynomial_order;

  std::vector<Real> _coefficients;

private:
};


#endif //COBALTTESTMATERIAL_H
