/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   3 February 2016
 *
 *****************************************************/

#ifndef COBALTBASEMATERIAL_H
#define COBALTBASEMATERIAL_H

#include "Material.h"

//forward declarations
class CobaltBaseMaterial;

template<>
InputParameters validParams<CobaltBaseMaterial>();

class CobaltBaseMaterial : public Material
{
public:
  CobaltBaseMaterial(const InputParameters & parameters);

protected:
  virtual void computeQpProperties();

  MaterialProperty<Real> & _L;
  MaterialProperty<Real> & _kappa_AC;

  MaterialProperty<Real> & _fbulk;
  MaterialProperty<Real> & _dfbulkdOP;
  MaterialProperty<Real> & _d2fbulkdOP2;

  //these store the values from the input file.
  Real _L_param;
  Real _w_param;
  Real _kappa_AC_param;

  const VariableValue & _OP;

private:
};


#endif //COBALTBASEMATERIAL_H
