/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   10 February 2016
 *
 *****************************************************/

#ifndef HETEROGENEOUSLINEARELASTICMATERIAL_H
#define HETEROGENEOUSLINEARELASTICMATERIAL_H

#include "Material.h"
#include "RankTwoTensor.h"
#include "RankFourTensor.h"

//forward declarations
class HeterogeneousLinearElasticMaterial;

template<>
InputParameters validParams<HeterogeneousLinearElasticMaterial>();

class HeterogeneousLinearElasticMaterial : public Material
{
public:
  HeterogeneousLinearElasticMaterial(const InputParameters & parameters);

protected:
  virtual void computeQpProperties();
  virtual void computeInterpolation();

  virtual void computeQpElasticityTensor();
  virtual void computeQpStrain();
  virtual void computeQpStress();

  virtual void computeEnergy();

  const VariableValue & _OP;

  const VariableGradient & _grad_disp_x;
  const VariableGradient & _grad_disp_y;
  const VariableGradient & _grad_disp_z;

  Real _h;
  Real _dhdn;
  Real _d2hdn2;

  MaterialProperty<RankTwoTensor> & _stress;

  MaterialProperty<RankTwoTensor> & _total_strain;
  MaterialProperty<RankTwoTensor> & _elastic_strain;
  MaterialProperty<RankTwoTensor> & _misfit_strain;

  MaterialProperty<RankFourTensor> & _elasticity_tensor;
  MaterialProperty<RankFourTensor> & _Jacobian_multiplier;

  MaterialProperty<Real> & _f_el;
  MaterialProperty<Real> & _dfel_dOP;
  MaterialProperty<Real> & _d2fel_dOP2;
  MaterialProperty<Real> & _f_el_int;

  RankFourTensor _Cijkl_matrix;
  RankFourTensor _Cijkl_precip;

  std::vector<Real> _eigenstrain_vector;

  RankTwoTensor _eigenstrain;

private:
};

#endif //HETEROGENEOUSLINEARELASTICMATERIAL_H
