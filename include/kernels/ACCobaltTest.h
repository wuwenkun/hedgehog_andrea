/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   3 February 2016
 *
 *****************************************************/

#ifndef ACCOBALTTEST_H
#define ACCOBALTTEST_H

#include "IsotropicACBulk.h"

//forward declarations
class ACCobaltTest;

template<>
InputParameters validParams<ACCobaltTest>();

class ACCobaltTest : public IsotropicACBulk
{
public:
  ACCobaltTest(const InputParameters & parameters);

protected:
  virtual Real computeDFDOP(PFFunctionType type);

private:
  const MaterialProperty<Real> & _df_dOP;
  const MaterialProperty<Real> & _d2f_dOP2;

};

#endif //ACCOBALTTEST_H
