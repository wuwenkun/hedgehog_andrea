/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   11 February 2016
 *
 *****************************************************/

#ifndef SPLITCHCOBALTELASTICITY_H
#define SPLITCHCOBALTELASTICITY_H

#include "SplitCHCobaltBase.h"

//forward declarations
class SplitCHCobaltElasticity;

template<>
InputParameters validParams<SplitCHCobaltElasticity>();

class SplitCHCobaltElasticity : public SplitCHCobaltBase
{
public:
  SplitCHCobaltElasticity(const InputParameters & parameters);

protected:
  virtual Real computeDFDC(PFFunctionType type);
  virtual Real computeDEDC(PFFunctionType type);

  const MaterialProperty<Real> & _dfel_dOP;
  const MaterialProperty<Real> & _d2fel_dOP2;

private:

};

#endif //SPLITCHCOBALTELASTICITY_H
