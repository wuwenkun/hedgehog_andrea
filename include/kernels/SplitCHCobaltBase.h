/******************************************************
 *
 *   Welcome to Hedgehog!
 *
 *   CHiMaD (ANL/Northwestern University)
 *
 *   Developer: Andrea Jokisaari
 *
 *   5 February 2016
 *
 *****************************************************/

#ifndef SPLITCHCOBALTBASE_H
#define SPLITCHCOBALTBASE_H

#include "SplitCHCRes.h"

//forward declarations
class SplitCHCobaltBase;

template<>
InputParameters validParams<SplitCHCobaltBase>();

class SplitCHCobaltBase : public SplitCHCRes
{
public:
  SplitCHCobaltBase(const InputParameters & parameters);

protected:
  virtual Real computeDFDC(PFFunctionType type);
//virtual Real computeDEDC(PFFunctionType type);

  const MaterialProperty<Real> & _df_dOP;
  const MaterialProperty<Real> & _d2f_dOP2;

private:

};

#endif //SPLITCHCOBALTBASE_H
