/******************************************************
*
*   Welcome to Hedgehog!
*
*   CHiMaD (ANL/Northwestern University)
*
*   Developer: Andrea Jokisaari
*
*   22 August 2016
*
*****************************************************/

#ifndef BLOB3IC_H
#define BLOB3IC_H

#include "InitialCondition.h"

// Forward Declarations
class Blob3IC;
namespace libMesh { class Point; }

template<>
InputParameters validParams<Blob3IC>();

class Blob3IC : public InitialCondition
{
public:
  Blob3IC(const InputParameters & parameters);
  virtual Real value(const Point & p);

protected:

private:
  Real _a;
  Real _b;
  Real _c;

  Real _invalue;
  Real _outvalue;

  Real _int_width;

//  Real _epsilon;
//  Real _base_value;

//  Point _q1;
//  Point _q2;
//  Point _q3;
//  Point _q4;

//  Real _k;
};

#endif //BLOB3IC_H
